//
//  SpeedometerPieChartView.swift
//  Charts
//
//  Created by Nosov Pavel on 7/24/17.
//
//

import UIKit

open class SpeedometerHalfPieChartView: PieChartView {

    open var pointerPercentValue:CGFloat = 0
    open var maxValue:CGFloat = 0
    open var maxValueFormat:String = "%.1f"
    
    internal override func initialize()
    {
        super.initialize()
        
        let halfPieRenderer = SpeedometerHalfPieChartRenderer(chart: self, animator: _animator, viewPortHandler: _viewPortHandler)
        renderer = halfPieRenderer

        _xAxis = nil
        
        self.highlighter = PieHighlighter(chart: self)
    }

}

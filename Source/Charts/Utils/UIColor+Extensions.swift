//
//  UIColor+Extensions.swift
//  CEOBoard
//
//  Created by Dmitri Utmanov on 12/11/15.
//  Copyright © 2015 1C-Rarus. All rights reserved.
//

import Foundation

fileprivate let gradientPercentage: CGFloat = 0.3

extension UIColor {

    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    
    func gradientColor() -> UIColor {
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 0.0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        var colorsArray = [r, g, b]
        for (index, colorComponent) in colorsArray.enumerated() {
            let dif = (1.0 - colorComponent) * gradientPercentage
            colorsArray[index] = colorComponent + dif
        }
        return UIColor(red: CGFloat(colorsArray[0]), green: CGFloat(colorsArray[1]), blue: CGFloat(colorsArray[2]), alpha: CGFloat(a))
    }

}

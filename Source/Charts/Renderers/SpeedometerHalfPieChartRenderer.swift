//
//  SpeedometerHalfPieChartRenderer.swift
//  Charts
//
//  Created by Nosov Pavel on 7/24/17.
//
//

import UIKit

open class SpeedometerHalfPieChartRenderer: PieChartRenderer {
    
    open override func drawData(context: CGContext) {
        super.drawData(context: context)
        guard let speedometerChart = chart as? SpeedometerHalfPieChartView else {
            return
        }
        drawPointer(value: speedometerChart.pointerPercentValue, context: context)
    }
    
    func drawPointer(value: CGFloat, context: CGContext) {
        
        guard let chart = chart else {
                return
        }
        
        let path = UIBezierPath()
        path.lineWidth = 5
        
        
        let bounds = CGRect(x: chart.centerCircleBox.x - chart.radius, y: chart.centerCircleBox.y - chart.radius, width: chart.radius * 2, height: chart.radius)
        let π = Double.pi
        
        let shape = CAShapeLayer()
        shape.frame = bounds

        if #available(iOS 13.0, *) {
            shape.fillColor = UIColor.label.cgColor
        } else {
            shape.fillColor = UIColor(r: 44/255.0, g: 54/255.0, b: 56/255.0, a: 1).cgColor
        }
        
        let shapeLayerBounds = shape.bounds
        
        let userInnerRadius = chart.radius * chart.holeRadiusPercent
        // Низ центр

        let margin:CGFloat = 10.0
        
        let rightPoint = CGPoint(x: shapeLayerBounds.midX + margin/2 , y: shapeLayerBounds.height - userInnerRadius - margin/2)
        path.move(to: rightPoint)
        
        // дуга
        path.addArc(withCenter: CGPoint(x: shapeLayerBounds.midX, y: shapeLayerBounds.height - userInnerRadius - margin/2), radius: margin/2, startAngle: CGFloat(0), endAngle: CGFloat(π), clockwise: true)
        // Верх центр
        path.addLine(to: CGPoint(x: shapeLayerBounds.midX, y:  -1.0*margin))
        path.close()
        
        let oldFrame = shape.frame
        shape.anchorPoint = CGPoint(x: 0.5, y: 1)
        shape.frame = oldFrame
        
        shape.path = path.cgPath
        
        let shapeLayerName = "pointer"
        shape.name = shapeLayerName
        chart.layer.sublayers?.forEach {
            if ($0.name == shapeLayerName) {
                $0.removeFromSuperlayer()
            }
        }
        
        chart.layer.addSublayer(shape)

        
        let calculatedValueToRotate = value/180.0*1.8
        let calculatedAngle = CGFloat(π) * CGFloat(calculatedValueToRotate)
        let baseAngle = CGFloat(-CGFloat(π) * 1.0 / 2.0)
        let rotateValue =  baseAngle + CGFloat(calculatedAngle)
        shape.transform = CATransform3DMakeRotation(CGFloat(rotateValue), 0, 0, 1)
        
        let x = chart.centerCircleBox.x - chart.radius
        let y = chart.centerCircleBox.y
        
        let font = UIFont.systemFont(ofSize: 17)
        let textColor: UIColor
        if #available(iOS 13.0, *) {
            textColor = UIColor.label
        } else {
            textColor = UIColor(r: 44/255.0, g: 54/255.0, b: 56/255.0, a: 1)
        }
        
        ChartUtils.drawText(
            context: context,
            text: "0",
            point: CGPoint(x: x, y: y),
            align: .left,
            attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: textColor]
        )
        
        let x1 = chart.centerCircleBox.x + chart.radius
        let y1 = chart.centerCircleBox.y
        
        guard let speedometerChart = chart as? SpeedometerHalfPieChartView else {
            return
        }
        
        ChartUtils.drawText(
            context: context,
            text: String(format: speedometerChart.maxValueFormat, speedometerChart.maxValue),
            point: CGPoint(x: x1, y: y1),
            align: .right,
            attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: textColor]
        )

    }
    
}
